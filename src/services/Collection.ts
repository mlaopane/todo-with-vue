export function getMaxId(collection: Array<{ id: number }>): number {
    function toMaxId(maxId: number, { id }: { id: number }) {
        if (maxId > id) {
            return maxId
        }
        return id
    }

    return collection.reduce(toMaxId, 0)
}
