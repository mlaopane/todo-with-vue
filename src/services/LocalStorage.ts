export default {
    setItem(key: string, payload: any) {
        localStorage.setItem(key, JSON.stringify(payload))
    },
    getItem(key: string) {
        const storageString = localStorage.getItem(key) as string

        try {
            return JSON.parse(storageString)
        } catch (error) {
            return null
        }
    },
}
