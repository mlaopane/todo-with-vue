export interface ITodoItem {
    id: number
    title: string
    completed: boolean
}

export type ITodoList = ITodoItem[]
