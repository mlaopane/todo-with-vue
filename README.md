# todo

## What is it ?

A todo app with basic features made with Vue.js just to experiment the framework.  
Try it out by visiting [https://mlaopane.gitlab.io/todo-with-vue/](https://mlaopane.gitlab.io/todo-with-vue/)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
