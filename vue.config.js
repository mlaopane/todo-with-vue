module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ?
        '/todo-with-vue/' :
        '/',
    outputDir: undefined,
    assetsDir: 'assets',
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined,
    lintOnSave: undefined,
}
